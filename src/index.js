//Sachin Dhalani - Near Transfer
import {
  NearContract,
  NearBindgen,
  near,
  call,
  view,
  bytes,
} from "near-sdk-js";

const HELLO_NEAR: string = "hello-nearverse.testnet";
const NO_DEPOSIT: number = 0;
const CALL_GAS: bigint = BigInt("5000000000000");
@NearBindgen
class Contract extends NearContract {
  constructor() {
    super();
  }
  // Transferring Near
  @call
  transfer({ to, amount }: { to: string, amount: BigInt }) {
    let promise = near.promiseBatchCreate(to);
    near.promiseBatchActionTransfer(promise, amount);
  }
  // Function Call
  @call
  call_method() {
    const args = bytes(JSON.stringify({ message: "howdy" }));
    const call = near.promiseBatchCreate(HELLO_NEAR);
    near.promiseBatchActionFunctionCall(
      call,
      "set_greeting",
      args,
      NO_DEPOSIT,
      CALL_GAS
    );
    const then = near.promiseThen(
      call,
      near.currentAccountId(),
      "callback",
      bytes(JSON.stringify({})),
      NO_DEPOSIT,
      CALL_GAS
    );
    return near.promiseReturn(then);
  }

  @call
  callback() {
    if (near.currentAccountId() !== near.predecessorAccountId()) {
      near.panic("This is a private method");
    }

    if (near.promiseResultsCount() == BigInt(1)) {
      near.log("Promise was successful!");
      return true;
    } else {
      near.log("Promise failed...");
      return false;
    }
  }
  //Sub Account
  @call
  create({ prefix }: { prefix: String }) {
    const account_id = `${prefix}.${near.currentAccountId()}`;
    const promise = near.promiseBatchCreate(account_id);
    near.promiseBatchActionCreateAccount(promise);
    near.promiseBatchActionTransfer(promise, MIN_STORAGE);
  }
  //   Creating Other Accounts
  create_account(
    { account_id, public_key } = { account_id: String, public_key: String }
  ) {
    const args = bytes(
      JSON.stringify({
        new_account_id: account_id,
        new_public_key: public_key,
      })
    );
  }

  //Add Product Details
  @call
  add_product({})
}
