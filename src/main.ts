//Sachin Dhalani - 25/08/2022
import { NearContract, NearBindgen, near, call, view, UnorderedMap } from 'near-sdk-js'
// If the user attaches more than 0.01N the message is premium
const PREMIUM_PRICE = BigInt('10000000000000000000000');

class Product{
    productId:string;
    isItemForRent: boolean;
    categoryId: string;
    subCategoryId: string;
    productName: string;
    price: number;
    liabilityPrice: number;
    images: Array<string>;
    sellerName: string;
    sellerAccountId: string;
    productAvailability: Array<string>;
    rentalDuration: number;
    bufferDuration: number;
    bufferPeriod: number;
    totalRentals: number;
    totalViews: number;
    favoritesAdded: number;
    isPublished: boolean;
    onRent: boolean;
    groupId: Array<string>;
     isPublic: boolean;

     constructor(){
        this.productId="1";
        this.isItemForRent=true;
        this.categoryId="1";
        this.subCategoryId="1";
        this.productName="Test";
        this.price=50;
        this.liabilityPrice=40;
        this.images=["test"];
        this.sellerName="";
        this.sellerAccountId="";
        this.productAvailability=["test"];
        this.rentalDuration=2;
        this.bufferDuration=3;
        this.bufferPeriod=1;
        this.totalRentals=10;
        this.totalViews=50;
        this.favoritesAdded=10;
        this.onRent=false;
        this.groupId=["test"];
        this.isPublic=true;
     }
}
@NearBindgen
class Contract extends NearContract{

productDetails:UnorderedMap;
constructor(){
    super()
    // this.products=[];
}
default() {
    return new Contract()
}
@call 
add_product({productId,isItemForRent,categoryId,subCategoryId,productName,price,liabilityPrice,images,sellerName,sellerAccountId,productAvailability,rentalDuration,bufferDuration,bufferPeriod,totalRentals,totalViews,favoritesAdded,isPublished,onRent,groupId,isPublic}:
    {productId:string,
    isItemForRent: boolean,
    categoryId: string,
    subCategoryId: string,
    productName: string,
    price: number,
    liabilityPrice: number,
    images: Array<string>,
    sellerName: string,
    sellerAccountId: string,
    productAvailability: Array<string>,
    rentalDuration: number,
    bufferDuration: number,
    bufferPeriod: number,
    totalRentals: number,
    totalViews: number,
    favoritesAdded: number,
    // productDetails: productDetails,
    isPublished: boolean,
    onRent: boolean,
    groupId: Array<string>,
     isPublic: boolean}){
       // this.products.push({productId,isItemForRent,categoryId,subCategoryId,productName,price,liabilityPrice,images,sellerName,sellerAccountId,productAvailability,rentalDuration,bufferDuration,bufferPeriod,totalRentals,totalViews,favoritesAdded,isPublished,onRent,groupId,isPublic});
       this.productDetails.set(productId,{productId,isItemForRent,categoryId,subCategoryId,productName,price,liabilityPrice,images,sellerName,sellerAccountId,productAvailability,rentalDuration,bufferDuration,bufferPeriod,totalRentals,totalViews,favoritesAdded,isPublished,onRent,groupId,isPublic});
       return {
        message: "Product Details Added",
        success: true,
      };
}
@call
update_product({productId,isItemForRent,categoryId,subCategoryId,productName,price,liabilityPrice,images,sellerName,sellerAccountId,productAvailability,rentalDuration,bufferDuration,bufferPeriod,totalRentals,totalViews,favoritesAdded,isPublished,onRent,groupId,isPublic}:
    {productId:string,
    isItemForRent: boolean,
    categoryId: string,
    subCategoryId: string,
    productName: string,
    price: number,
    liabilityPrice: number,
    images: Array<string>,
    sellerName: string,
    sellerAccountId: string,
    productAvailability: Array<string>,
    rentalDuration: number,
    bufferDuration: number,
    bufferPeriod: number,
    totalRentals: number,
    totalViews: number,
    favoritesAdded: number,
    // productDetails: productDetails,
    isPublished: boolean,
    onRent: boolean,
    groupId: Array<string>,
     isPublic: boolean}){
         let accountExist = this.productDetails.get(productId);
         near.log(accountExist);
         this.productDetails.set(productId,{productId,isItemForRent,categoryId,subCategoryId,productName,price,liabilityPrice,images,sellerName,sellerAccountId,productAvailability,rentalDuration,bufferDuration,bufferPeriod,totalRentals,totalViews,favoritesAdded,isPublished,onRent,groupId,isPublic});
         return {
            message: "Product Details Updated",
            success: true,
          };
}

@call 
delete_product({productId,isItemForRent,categoryId,subCategoryId,productName,price,liabilityPrice,images,sellerName,sellerAccountId,productAvailability,rentalDuration,bufferDuration,bufferPeriod,totalRentals,totalViews,favoritesAdded,isPublished,onRent,groupId,isPublic}:
    {productId:string,
    isItemForRent: boolean,
    categoryId: string,
    subCategoryId: string,
    productName: string,
    price: number,
    liabilityPrice: number,
    images: Array<string>,
    sellerName: string,
    sellerAccountId: string,
    productAvailability: Array<string>,
    rentalDuration: number,
    bufferDuration: number,
    bufferPeriod: number,
    totalRentals: number,
    totalViews: number,
    favoritesAdded: number,
    // productDetails: productDetails,
    isPublished: boolean,
    onRent: boolean,
    groupId: Array<string>,
     isPublic: boolean}){
         let accountExist = this.productDetails.get(productId);
         near.log(accountExist);
         this.productDetails.remove(productId);
         return {
            message: "Product Details Deleted",
            success: true,
          };

}
@view
display(productId:string){
    let accountExist = this.productDetails.get(productId);
    near.log(accountExist);
    this.productDetails.values;
}
}
